<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';


if( !bab_isUserAdministrator())
{
    global $babBody;
    $babBody->addError("Access denied");
    return false;
}


@bab_functionality::includefile('Icons');


//Chargement des fonctions
require_once dirname(__FILE__).'/fonctionsgen.php';

function ovmlport_list(){
    global $babBody, $babDB;
    require_once $GLOBALS['babInstallPath']."utilit/dateTime.php";

    $W = ovmlport_Widgets();

    $page = $W->BabPage();
    $page->addItemMenu('list', ovmlport_translate('Portlets OVML list'), '?tg=addon/OVMLPortlets/main');
    $page->addItemMenu('edit', ovmlport_translate('Add a OVML Portlet'), '?tg=addon/OVMLPortlets/main&idx=edit');
    $page->setCurrentItemMenu('list');
    $page->addItem(
        $W->Html(
            '<style type="text/css">
                #search{
                    background-color: #EEE;
                    color: #666;
                    padding: 10px 0 10px 0px;
                    text-align: center;
                }
                .ur_header, .ur_header:hover{
                    background-color: #CCC;
                }
                #ur_remove_btn{
                    margin: 10px;
                }
                #side-note{
                    color: #556677;
                    text-shadow: 0 1px 0 #FFFFFF;
                    font-size: 0.9em;
                    font-weight: normal
                }
            </style>'
            )
        );


    if(bab_gp('save','') == 1){
        $page->addError(ovmlport_translate('Update done'));
        $_POST['date'] = bab_gp('date');
    }


    $list = $W->TableView();
    $list->addRowClass(0, 'ur_header');
    $list->addItem($W->Label(ovmlport_translate('Name')),0,0);
    $list->addItem($W->Label(ovmlport_translate('Description')),0,1);
    $list->addItem($W->Label(ovmlport_translate('')),0,3);

    $sql = "SELECT * FROM ovmlport_portlets WHERE deleted = ".$babDB->quote(bab_rp('deleted', 0))." ORDER BY name ASC";
    $res = $babDB->db_query($sql);
    $j = 1;
    while ($res && ($ports = $babDB->db_fetch_assoc($res))) {
        $list->addItem($W->Label($ports['name']),$j,0);
        $list->addItem($W->Label($ports['description']),$j,1);
        if(bab_rp('deleted') == 1){
            $deleteAction = $W->Link($W->Icon('',Func_Icons::APPS_APPROBATIONS), '?tg=addon/OVMLPortlets/main&idx=enable&id='.$ports['id'])->setTitle('Activer');
        }else{
            $deleteAction = $W->Link($W->Icon('',Func_Icons::ACTIONS_EDIT_DELETE), '?tg=addon/OVMLPortlets/main&idx=remove&id='.$ports['id'])->setTitle('Supprimer')->setConfirmationMessage('Voulez vous vraiment supprimer ce Portlet?');
        }
        $list->addItem(
            $W->HBoxItems(
                $W->Link($W->Icon('',Func_Icons::ACTIONS_SET_ACCESS_RIGHTS), '?tg=addon/OVMLPortlets/main&idx=rights&item='.$ports['id'])->setTitle('Droits'),
                $W->Link($W->Icon('',Func_Icons::ACTIONS_DOCUMENT_EDIT), '?tg=addon/OVMLPortlets/main&idx=edit&id='.$ports['id'])->setTitle('Modifier'),
                $W->Link($W->Icon('',Func_Icons::ACTIONS_EDIT_COPY), '?tg=addon/OVMLPortlets/main&idx=clone&id='.$ports['id'])->setTitle('Dupliquer'),
                $deleteAction
                )->addClass(Func_Icons::ICON_LEFT_16),$j,3
            );
        $j++;
    }

    $page->addItem($list);

    $page->pageEcho($W->HtmlCanvas());
}

function ovmlport_edit(){
    global $babBody, $babDB;
    include_once $GLOBALS['babInstallPath']."admin/acl.php";

    $addonInfo = bab_getAddonInfosInstance('OVMLPortlets');

    if( !bab_isUserAdministrator())
    {
        $babBody->addError(ovmlport_translate("Access denied"));
        return false;
    }

    $W = ovmlport_Widgets();

    $page = $W->BabPage();
    $page->addJavascriptFile($GLOBALS['babInstallPath']."scripts/bab_dialog.js");
    $page->addJavascriptFile($addonInfo->getTemplatePath()."ovml_portlets.js");

    $page->addItem(
        $W->Html(
            '<style type="text/css">
                #how-to-text
                {
                    font-family: Arial, sans-serif;
                    font-size:11px;
                    border:#999 2px solid;
                    background-color:#EEE;
                    padding:5px;
                    position:absolute;
                    text-align:left;
                    visibility:hidden;
                    width:500px;
                    z-index:100;
                    max-height:300px;
                    overflow:hidden;
                }
                #text-100{
                    width: 100%;
                }
            </style>'
            .ovmlport_translate("_setting1_")."<br \>"
            .ovmlport_translate("_setting2_")."<br \>"
            .ovmlport_translate("_setting3_")."<br \>"
            .ovmlport_translate("_setting4_")."<br \>"
            .ovmlport_translate("_setting5_")."<br \>"
            .ovmlport_translate("_setting6_")."<br \>"
            .ovmlport_translate("_setting7_")."<br \>",
            'how-to-text'
            )
        );
    $page->addItemMenu('list', ovmlport_translate('Portlets OVML list'), '?tg=addon/OVMLPortlets/main');
    $page->addItemMenu('edit', ovmlport_translate('Add a OVML Portlet'), '?tg=addon/OVMLPortlets/main&idx=edit');
    $page->setCurrentItemMenu('edit');
    $page->addItem(
        $W->Title(
            ovmlport_translate('Add a OVML Portlet'),
            1
            )
        );
    if(bab_gp('save','') == 1){
        $page->addError(ovmlport_translate('Update done'));
    }

    $From = $W->Form()
    ->setHiddenValue('tg', bab_rp('tg'))
    ->setHiddenValue('idx', 'save')
    ->addItem(
        $W->VBoxItems(
            $W->VBoxItems(
                $tempLbl = $W->Label(ovmlport_translate('Name').' :'),
                $W->LineEdit()
                ->setSize(50)
                ->setMandatory(true,ovmlport_translate("The field 'name' is mandatory."))
                ->setName('name')
                ->setAssociatedLabel($tempLbl)
                ),
            $W->VBoxItems(
                $tempLbl = $W->Label(ovmlport_translate('Cache life in second (0 = no cache)').' :'),
                $W->LineEdit()->setValue(0)
                ->setSize(50)
                ->setMandatory(true,ovmlport_translate("The field 'cache' is mandatory."))
                ->setName('cached_time')
                ->setAssociatedLabel($tempLbl)
                ),
            $W->VBoxItems(
                $tempLbl = $W->Label(ovmlport_translate('Description').' :'),
                $W->TextEdit()
                ->setMandatory(true,ovmlport_translate("The field 'description' is mandatory."))
                ->setName('description')
                ->setAssociatedLabel($tempLbl)
                ),
            $W->VBoxItems(
                $tempLbl = $W->Label(ovmlport_translate('OVML').' :'),
                $W->TextEdit('text-100')->setLines(25)->setSizePolicy(Widget_SizePolicy::MAXIMUM)
                ->setMandatory(true,ovmlport_translate("The field OVML' is mandatory."))
                ->setName('ovml')
                ->setAssociatedLabel($tempLbl)
                ),
            $W->VBoxItems(
                $W->HBoxItems(
                    $tempLbl = $W->Label(ovmlport_translate('OVML Options').' :'),
                    $W->Icon('',Func_Icons::STATUS_DIALOG_INFORMATION, 'ovml-how-to')
                    )->addClass(Func_Icons::ICON_LEFT_16),
                $optionW = $W->TextEdit()
                ->setName('options')
                ->setAssociatedLabel($tempLbl)
                ),
            $buttonFrame = $W->HboxItems(
                $W->SubmitButton()->setLabel(ovmlport_translate('Save'))->setName('valid'),
                $W->SubmitButton()->setLabel(ovmlport_translate('Cancel'))->setName('cancel')
                )->setHorizontalSpacing(1,'em')
            )->setVerticalAlign('left')->setVerticalSpacing(.5,'em')
        );



    $id = bab_rp('id','');
    if($id != ''){
        $res = $babDB->db_query("SELECT * from ovmlport_portlets WHERE id=".$babDB->quote($id));
        $arr = $babDB->db_fetch_array($res);

        $From->setHiddenValue('id', $arr['id']);
        $From->setValues($arr);

        $res = $babDB->db_query("SELECT * from ovmlport_options WHERE id_portlet=".$babDB->quote($id));
        $options = '';
        while($arr = $babDB->db_fetch_array($res)){
            $options.= $arr['name'].','.$arr['type'].','.$arr['description']."\n";
        }
        $optionW->setValue($options);
    }

    $page->addItem($From);

    $page->pageEcho($W->HtmlCanvas());
}



function ovmlport_save(){
    global $babDB;

    if(bab_pp('id','') != ''){
        $babDB->db_query("
            UPDATE ovmlport_portlets
            SET name = ".$babDB->quote(bab_pp('name','')).",
                description = ".$babDB->quote(bab_pp('description','')).",
                ovml = ".$babDB->quote(bab_pp('ovml','')).",
                cached_time = ".$babDB->quote(bab_pp('cached_time',''))."
            WHERE id = ".$babDB->quote(bab_pp('id',''))
            );
        $id = bab_pp('id','');
    }else{
        $babDB->db_query("
            INSERT INTO ovmlport_portlets (name, description, ovml, cached_time)
            VALUES (
                ".$babDB->quote(bab_pp('name','')).",
                ".$babDB->quote(bab_pp('description','')).",
                ".$babDB->quote(bab_pp('ovml','')).",
                ".$babDB->quote(bab_pp('cached_time',''))."
            )
        ");
        $id = $babDB->db_insert_id();
    }

    $babDB->db_query("
        DELETE FROM ovmlport_options
        WHERE id_portlet = ".$babDB->quote($id)
        );

    $options = bab_pp('options','');
    if($options != ''){
        $lines = explode("\n", $options);
        foreach($lines as $line){
            $arrayOptions = explode(",", $line);
            if($arrayOptions['0'] != '' && $arrayOptions['1'] != ''&& $arrayOptions['2'] != ''){
                $babDB->db_query("
                    INSERT INTO ovmlport_options (id_portlet, name, type, description)
                    VALUES (
                        ".$babDB->quote($id).",
                        ".$babDB->quote(trim($arrayOptions['0'])).",
                        ".$babDB->quote(trim($arrayOptions['1'])).",
                        ".$babDB->quote(trim($arrayOptions['2']))."
                    )
                ");
            }
        }
    }

    header('Location: ' . '?tg=addon/OVMLPortlets/main');
    exit;
}

function ovmlport_remove(){
    global $babDB;

    $id = bab_gp('id','');

    if($id){
        $babDB->db_query("
            UPDATE ovmlport_portlets
            SET deleted = 1
            WHERE id = ".$babDB->quote($id)
            );
    }

    header('Location: ' . '?tg=addon/OVMLPortlets/main');
    exit;
}

function ovmlport_enable(){
    global $babDB;

    $id = bab_gp('id','');

    if($id){
        $babDB->db_query("
            UPDATE ovmlport_portlets
            SET deleted = 0
            WHERE id = ".$babDB->quote($id)
            );
    }

    header('Location: ' . '?tg=addon/OVMLPortlets/main');
    exit;
}

function ovmlport_clone(){
    global $babDB;
    if(bab_gp('id','') != ''){
        $res = $babDB->db_query("
            SELECT * FROM ovmlport_portlets
            WHERE id = ".$babDB->quote(bab_gp('id',''))
            );

        $port = $babDB->db_fetch_assoc($res);

        $babDB->db_query("
            INSERT INTO ovmlport_portlets (name, description, ovml)
            VALUES (
                ".$babDB->quote('(Copie) '.$port['name']).",
                ".$babDB->quote($port['description']).",
                ".$babDB->quote($port['ovml'])."
            )
        ");
        $idClone = $babDB->db_insert_id();
    }

    $res = $babDB->db_query("
        SELECT * FROM ovmlport_options
        WHERE id_portlet = ".$babDB->quote(bab_gp('id'))
        );



    while($line = $babDB->db_fetch_assoc($res)){
        $babDB->db_query("
            INSERT INTO ovmlport_options (id_portlet, name, type, description)
            VALUES (
                ".$babDB->quote($idClone).",
                ".$babDB->quote($line['name']).",
                ".$babDB->quote($line['type']).",
                ".$babDB->quote($line['description'])."
            )
        ");
    }

    header('Location: ' . '?tg=addon/OVMLPortlets/main');
    exit;
}



function ovmlport_rights(){
    global $babBody;
    include_once $GLOBALS['babInstallPath']."admin/acl.php";

    if( !bab_isUserAdministrator())
    {
        $babBody->addError(ovmlport_translate("Access denied"));
        return false;
    }


    if( isset($_POST['updateaccess']))
    {
        maclGroups();
        $babBody->addError(ovmlport_translate("Update done"));
    }

    $babBody->setTitle(ovmlport_translate('Rights'));
    $babBody->addItemMenu('list', ovmlport_translate('Portlets OVML list'), '?tg=addon/OVMLPortlets/main');
    $babBody->addItemMenu('right', ovmlport_translate('Rights'),'');
    $babBody->setCurrentItemMenu('right');

    $macl = new macl(bab_rp('tg'), 'rights', bab_rp('item'), 'updateaccess');
    $macl->addtable( 'ovmlport_access_groups',ovmlport_translate('Who can view this portlet?'));
    $macl->filter(0,0,0,0,0);
    $macl->babecho();
}



$idx = bab_rp('idx','list');

switch($idx){
    default:
    case 'list':
        ovmlport_list();
        break;
    case 'clone':
        ovmlport_clone();
        break;
    case 'edit':
        ovmlport_edit();
        break;
    case 'save':
        ovmlport_save();
        break;
    case 'remove':
        ovmlport_remove();
        break;
    case 'enable':
        ovmlport_enable();
        break;
    case 'rights':
        ovmlport_rights();
        break;
}