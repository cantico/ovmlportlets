CREATE TABLE `ovmlport_portlets` (
	`id` int(11) unsigned NOT NULL auto_increment,
	`name` tinytext NOT NULL,
	`description` text NOT NULL default '',
	`ovml` text NOT NULL default '',
	`cached_time` int(11) NOT NULL default 0,
	`deleted` bool NOT NULL default false,
	PRIMARY KEY  (`id`)
);

CREATE TABLE `ovmlport_options` (
	`id` int(11) unsigned NOT NULL auto_increment,
	`id_portlet` int(11) unsigned NOT NULL,
	`type` varchar(127) default 'text',
	`name` varchar(127) NOT NULL default '',
	`description` varchar(127) NOT NULL default '',
	PRIMARY KEY  (`id`)
);

INSERT INTO `ovmlport_options` (`id_portlet`, `type`, `name`, `description`) VALUES
(1, 'text', 'nbRow', 'Nombre d''articles'),
(1, 'text', 'categoryID', 'Identifiant(s) de catégorie'),
(2, 'text', 'idCal', 'Identifiant(s) des agendas'),
(2, 'text', 'nbEvent', 'Nombre d''évènements'),
(3, 'text', 'nbFile', 'Nombre de fichiers');



INSERT INTO `ovmlport_access_groups` (`id_object`, `id_group`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0);


INSERT INTO `ovmlport_portlets` (`id`, `name`, `description`, `ovml`, `cached_time`, `deleted`) VALUES
(1,	'Articles récents',	'Portlet OVML générique',	'<div class=\"ovmlportlets-recentarticles\">\r\n<h2>Les derniers articles</h2>\r\n\r\n<OCIfLessThanOrEqual expr1=\"<OVnbRow>\" expr2=\"0\">\r\n<OFPutVar name=\"nbRow\" value=\"10\">\r\n</OCIfLessThanOrEqual>\r\n\r\n<OCRecentArticles last=\"<OVnbRow>\" categoryid=\"<OVcategoryID>\">\r\n<div class=\"article bab-article-<OVArticleId>\">\r\n<a class=\"title\" href=\"<OVArticleUrl htmlentities=\"1\">\"><OVArticleTitle htmlentities=\"1\"></a><br />\r\n<span class=\"date\">le <OVArticleDate date=\"%j %M %Y\"></span> <span class=\"hour\">à <OVArticleDate date=\"%H:%i\"></span>\r\n</div>\r\n</OCRecentArticles>\r\n</div>\r\n',	0,	0),
(2,	'Evénements récents',	'Portlet OVML générique',	'<div class=\"ovmlportlets-calendarevents\">\r\n<h2>Les prochains &eacute;v&eacute;nements</h2>\r\n\r\n<OCIfLessThanOrEqual expr1=\"<OVnbEvent>\" expr2=\"0\">\r\n<OFPutVar name=\"nbEvent\" value=\"10\">\r\n</OCIfLessThanOrEqual>\r\n\r\n<ul>\r\n<OCCalendarEvents limit=\"0,<OVnbEvent>\" calendarid=\"<OVidCal>\">\r\n<li>\r\n<a class=\"titre-element\" href=\"<OVEventUrl htmlentities=\"1\">\" onclick=\"bab_popup(this.href); return false;\">\r\n<OVEventTitle htmlentities=\"1\">\r\n</a>\r\n<br />\r\n<span class=\"date\">\r\nle <OVEventBeginDate date=\"%j %M %Y\"></span> <span class=\"hour\">&agrave; <OVEventBeginDate date=\"%H:%i\">\r\n</span>\r\n</li>\r\n</OCCalendarEvents> \r\n</ul>\r\n</div>',	0,	0),
(3,	'Fichiers récents',	'Portlet OVML générique',	'<div class=\"ovmlportlets-recentfiles\">\r\n<h2>Les derniers fichiers</h2>\r\n\r\n<OCIfLessThanOrEqual expr1=\"<OVnbFile>\" expr2=\"0\">\r\n<OFPutVar name=\"nbFile\" value=\"10\">\r\n</OCIfLessThanOrEqual>\r\n\r\n<ul>\r\n<OCRecentFiles last=\"<OVnbFile>\">\r\n<li>\r\n<a class=\"title\" href=\"<OVFileUrlGet htmlentities=\"1\">\"><OVFileName strlen=\"45,...\" htmlentities=\"1\"></a><br />\r\n<a href=\"<OVFilePopupUrl htmlentities=\"1\">\" onclick=\"bab_popup(this.href); return false;\">voir les infos</a> | <a href=\"<OVFileUrlGet htmlentities=\"1\">\">t&eacute;l&eacute;charger</a>\r\n</li>\r\n</OCRecentFiles> \r\n</ul>\r\n</div>',	0,	0),
(4,	'Articles de la page d\'accueil',	'Portlet OVML générique',	'<OFPutVar name=\"BAB_SESS_LOGGED\">\r\n<OFPutVar name=\"babLanguage\">\r\n<OFPutVar name=\"Class\" value=\"ovidentia_publichomepage\">\r\n<OCIfEqual expr1=\"<BAB_SESS_LOGGED>\" expr2=\"1\">\r\n<OFPutVar name=\"Class\" value=\"ovidentia_privatehomepage\">\r\n</OCIfEqual>\r\n<OCArticlesHomePages filter=\"yes\">\r\n<OCIfEqual expr1=\"<OVArticleLanguage>\" expr2=\"<OVbabLanguage>\">\r\n<div class=\"<OVClass>_article\">\r\n<h3><OVArticleTitle htmlentities=\"1\"></h3>\r\n<div class=\"<OVClass>_articlehead\"><OVArticleHead></div>\r\n</div>\r\n</OCIfEqual>\r\n</OCArticlesHomePages>',	0,	0);
