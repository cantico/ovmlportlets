; <?php /*
[general]
name="OVMLPortlets"
version="1.6.5"
addon_type="EXTENSION"
encoding="UTF-8"
mysql_character_set_database = "latin1,utf8"
description="OVML portlets managements with acces by groups"
description.fr="Module permettant de créer des portlets en OVML"
long_description.fr="README.md"
db_prefix="ovmlport_"
delete=1
ov_version="8.1.98"
author="antgal@cantico.fr"
icon="icon.jpg"
addon_access_control="0"
tags="extension,default"

[addons]
widgets				="1.0.4"
LibOrm				="0.7.8"

[functionalities]
jquery				="Available"
; */ ?>
