<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__).'/fonctionsgen.php';

function OVMLPortlets_onDeleteAddon()
{
    $addon = bab_getAddonInfosInstance('OVMLPortlets');
    $addon->removeEventListener('bab_eventPageRefreshed', 'OVMLPortlets_onPageRefreshed', 'init.php');

    $addonPhpPath = $addon->getPhpPath();
    require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
    $functionalities = new bab_functionalities();
    $functionalities->unregister('PortletBackend/ovml');
}

function OVMLPortlets_upgrade($version_base, $version_ini)
{
    global $babDB;

    require_once $GLOBALS['babInstallPath'] . 'utilit/upgradeincl.php';
    require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';

    $access = new bab_synchronizeSql();
    $access->setDisplayMessage(false);
    $access->fromSqlFile(dirname(__FILE__).'/data/access.sql');

    $addon = bab_getAddonInfosInstance('OVMLPortlets');

    $addon->addEventListener('bab_eventPageRefreshed', 'OVMLPortlets_onPageRefreshed', 'init.php');

    if(!bab_isTable('ovmlport_portlets') || !bab_isTable('ovmlport_options')){
        if (bab_execSqlFile(dirname(__FILE__).'/data/portlet.sql', bab_Charset::UTF_8)) {
            bab_installWindow::message(bab_toHtml('Initialization... done'));
        }
    }

    if(!bab_isTableField('ovmlport_portlets', 'deleted')){
        $babDB->db_query(bab_convertToDatabaseEncoding("
            ALTER TABLE `ovmlport_portlets` ADD `deleted` BOOL NOT NULL DEFAULT '0';
        "));
    }

    if(!bab_isTableField('ovmlport_portlets', 'cached_time')){
        $babDB->db_query(bab_convertToDatabaseEncoding("
            ALTER TABLE `ovmlport_portlets` ADD `cached_time` int(11) NOT NULL default 0;
        "));
    }

    @bab_functionality::includefile('PortletBackend');
    if (class_exists('Func_PortletBackend')) {

        $addonPhpPath = $addon->getPhpPath();
        require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
        $functionalities = new bab_functionalities();
        $functionalities->registerClass('Func_PortletBackend_ovml', $addonPhpPath . 'portletbackend.class.php');
    }

    $addon->addEventListener('bab_eventBeforeSiteMapCreated', 'OVMLPortlets_onSiteMapItems', 'init.php');

    return true;
}


/**
 * Sitemap creation
 * @param bab_eventBeforeSiteMapCreated $event
 * @return mixed
 */
function OVMLPortlets_onSiteMapItems(bab_eventBeforeSiteMapCreated $event) {
    bab_functionality::includefile('Icons');
    if (bab_isUserAdministrator()) {
        $link = $event->createItem('OVMLPortlets_Admin');
        $link->setLabel(ovmlport_translate('OVMLPortlets'));
        $link->setLink('?tg=addon/OVMLPortlets/main');
        $link->setPosition(array('root', 'DGAll', 'babAdmin', 'babAdminSectionAddons'));
        $link->addIconClassname(Func_Icons::PLACES_ADMINISTRATOR_APPLICATIONS);

        $event->addFunction($link);
    }

}




function OVMLPortlets_onPageRefreshed()
{
    if ('addon/OVMLPortlets/main' === bab_rp('tg')) {
        $jquery = bab_functionality::get('jquery');
        $jquery->includeCore();
    }
}
