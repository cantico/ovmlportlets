<?php

/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2008 by CANTICO ( http://www.cantico.fr )              *
 ************************************************************************/
include_once 'base.php';

//Chargement des fonctions
global $babInstallPath;
include_once dirname(__FILE__).'/fonctionsgen.php';


/**
 * Management of containers OVML
 *
 * @param Array $args
 * @return Array
 */
function applications_ovml($args) {
    $donnees = Array();

    if (isset($args['action'])) {
        switch($args['action']){
            case 'Applications':
                /* Recupere les informations de l'application */
                $requete = "select id, url, shortdesc, longdesc, description, display_mode, place from app_list";
                /* Verifie si des filtres sont demandes */
                $where = false;
                if (isset($args['place'])) {
                    switch($args['place']) {
                        case '1':
                            $requete = $requete." where place='1'";
                            $where = true;
                            break;
                        case '2':
                            $requete = $requete." where place='2'";
                            $where = true;
                            break;
                        case '3':
                            $requete = $requete." where place='3'";
                            $where = true;
                            break;
                    }
                }
                if (isset($args['displaymode'])) {
                    switch($args['displaymode']) {
                        case '1':
                            if ($where) {
                                $requete = $requete." and display_mode='new_window'";
                            } else {
                                $requete = $requete." where display_mode='new_window'";
                            }
                            break;
                        case '2':
                            if ($where) {
                                $requete = $requete." and display_mode='body'";
                            } else {
                                $requete = $requete." where display_mode='body'";
                            }
                            break;
                        case '3':
                            if ($where) {
                                $requete = $requete." and display_mode='iframe'";
                            } else {
                                $requete = $requete." where display_mode='iframe'";
                            }
                            break;
                    }
                }
                $requete = $requete.' order by shortdesc asc'; /* ordre alphabetique sur le nom */

                $idrequete = 0;
                $erreurs = Array();
                $donnees = applications_sql($requete, $erreurs, $idrequete);
                if (count($donnees) != 0) {
                    for ($i=0;$i<=count($donnees)-1;$i++) {
                        /* Creation des variables OVML du container */
                        $tab = Array();
                        /* Test des droits d'acces */
                        if (bab_isAccessValid('app_groups', $donnees[$i]['id'])) {
                            $tab['ApplicationId'] = $donnees[$i]['id'];
                            if ($donnees[$i]['display_mode'] == 'iframe') {
                                global $babAddonUrl;
                                $tab['ApplicationUrl'] = $babAddonUrl.'main&amp;idx=iframe&amp;id_app='.$donnees[$i]['id'];
                            } else {
                                $tab['ApplicationUrl'] = $donnees[$i]['url'];
                            }
                            $tab['ApplicationName'] = $donnees[$i]['shortdesc'];
                            $tab['ApplicationLongName'] = $donnees[$i]['longdesc'];
                            $tab['ApplicationDescription'] = $donnees[$i]['description'];
                            $tab['ApplicationPlace'] = $donnees[$i]['place'];
                            switch($donnees[$i]['place']) {
                                case '1':
                                    $tab['ApplicationPlaceText'] = applications_traduire('In specific section');
                                    break;
                                case '2':
                                    $tab['ApplicationPlaceText'] = applications_traduire('In user section');
                                    break;
                                case '3':
                                    $tab['ApplicationPlaceText'] = applications_traduire('No place');
                                    break;
                                default:
                                    $tab['ApplicationPlaceText'] = '';
                                    break;
                            }
                            switch($donnees[$i]['display_mode']) {
                                case 'new_window':
                                    $tab['ApplicationDisplayMode'] = '1';
                                    $tab['ApplicationDisplayModeText'] = applications_traduire('New window');
                                    break;
                                case 'body':
                                    $tab['ApplicationDisplayMode'] = '2';
                                    $tab['ApplicationDisplayModeText'] = applications_traduire('Same window');
                                    break;
                                case 'iframe':
                                    $tab['ApplicationDisplayMode'] = '3';
                                    $tab['ApplicationDisplayModeText'] = applications_traduire('In Ovidentia body');
                                    break;
                                default:
                                    $tab['ApplicationDisplayMode'] = '';
                                    $tab['ApplicationDisplayModeText'] = '';
                                    break;
                            }
                            $res[] = $tab;
                        }
                    }
                    return $res;
                }
                break;
            case 'Application':
                /* Verifie l'identifiant de l'application et les droits d'acces */
                if (isset($args['applicationid'])) {
                    /* Recupere les informations de l'application */
                    $applicationid = $args['applicationid'];
                    $requete = "select id, url, shortdesc, longdesc, description, display_mode, place from app_list WHERE id = '$applicationid'";
                    $idrequete = 0;
                    $erreurs = Array();
                    $donnees = applications_sql($requete, $erreurs, $idrequete);
                    if (count($donnees) == 1) {
                        /* Creation des variables OVML du container */
                        $tab = Array();
                        /* Test des droits d'acces */
                        if (bab_isAccessValid('app_groups', $donnees[0]['id'])) {
                            $tab['ApplicationId'] = $donnees[0]['id'];
                            if ($donnees[0]['display_mode'] == 'iframe') {
                                global $babAddonUrl;
                                $tab['ApplicationUrl'] = $babAddonUrl.'main&amp;idx=iframe&amp;id_app='.$donnees[0]['id'];
                            } else {
                                $tab['ApplicationUrl'] = $donnees[0]['url'];
                            }
                            $tab['ApplicationName'] = $donnees[0]['shortdesc'];
                            $tab['ApplicationLongName'] = $donnees[0]['longdesc'];
                            $tab['ApplicationDescription'] = $donnees[0]['description'];
                            $tab['ApplicationPlace'] = $donnees[0]['place'];
                            switch($donnees[0]['place']) {
                                case '1':
                                    $tab['ApplicationPlaceText'] = applications_traduire('In specific section');
                                    break;
                                case '2':
                                    $tab['ApplicationPlaceText'] = applications_traduire('In user section');
                                    break;
                                case '3':
                                    $tab['ApplicationPlaceText'] = applications_traduire('No place');
                                    break;
                                default:
                                    $tab['ApplicationPlaceText'] = '';
                                    break;
                            }
                            switch($donnees[0]['display_mode']) {
                                case 'new_window':
                                    $tab['ApplicationDisplayMode'] = '1';
                                    $tab['ApplicationDisplayModeText'] = applications_traduire('New window');
                                    break;
                                case 'body':
                                    $tab['ApplicationDisplayMode'] = '2';
                                    $tab['ApplicationDisplayModeText'] = applications_traduire('Same window');
                                    break;
                                case 'iframe':
                                    $tab['ApplicationDisplayMode'] = '3';
                                    $tab['ApplicationDisplayModeText'] = applications_traduire('In Ovidentia body');
                                    break;
                                default:
                                    $tab['ApplicationDisplayMode'] = '';
                                    $tab['ApplicationDisplayModeText'] = '';
                                    break;
                            }
                            $res[] = $tab;
                        }
                        return $res;
                    }
                }
                break;
        }
    }

    return $donnees;
}

?>
