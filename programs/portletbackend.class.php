<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
global $babInstallPath;
include_once $babInstallPath.'admin/acl.php';

require_once dirname(__FILE__).'/fonctionsgen.php';

bab_Functionality::includefile('PortletBackend');

/**
 *
 */
class Func_PortletBackend_ovml extends Func_PortletBackend
{
	public function getDescription()
	{
		return ovmlport_translate('OVML Portlets');
	}

	public function select($category = null)
	{
		if (null !== $category)
		{
			return array();
		}
		
		$addon = bab_getAddonInfosInstance('OVMLPortlets');
		if (!$addon->isAccessValid()) {
		    return array();
		}
		
		
		global $babDB;
		$res = $babDB->db_query("SELECT a.* from ovmlport_portlets a WHERE deleted = 0 ORDER BY name ASC");
		$listApp = array();
		while($arr = $babDB->db_fetch_array($res)){
			if(bab_isAccessValid('ovmlport_access_groups',$arr['id'])){
				$listApp['ovml_'.$arr['id']] = $this->portlet_OVML($arr['id'], $arr['name'], $arr['description']);
			}
		}

		return $listApp;
	}



	/**
	 * Get portlet definition instance
	 * @param	string	$portletId			portlet definition ID
	 *
	 * @return portlet_PortletDefinitionInterface
	 */
	public function getPortletDefinition($portletId)
	{
	    $addon = bab_getAddonInfosInstance('OVMLPortlets');
	    if (!$addon->isAccessValid()) {
	        return null;
	    }
	    
		$portletId = str_replace('ovml_', '', $portletId);

		global $babDB;
		$res = $babDB->db_query("SELECT a.* from ovmlport_portlets a WHERE deleted = 0 AND id=".$babDB->quote($portletId)." ORDER BY name ASC");
		$arr = $babDB->db_fetch_array($res);
		if(bab_isAccessValid('ovmlport_access_groups',$arr['id'])){
			return $this->portlet_OVML($arr['id'], $arr['name'], $arr['description']);
		}

		return null;
	}


	public function portlet_OVML($id, $name, $description)
	{
		return new PortletDefinition_OVML($id, $name, $description);
	}

	public function getConfigurationActions()
	{
		if( !bab_isUserAdministrator())
		{
			return array();
		}
		
		$addon = bab_getAddonInfosInstance('OVMLPortlets');
		
		$W = bab_functionality::get('Widgets');
		/*@var $W Func_Widgets */
		
		return array(
			$W->Action()->fromUrl($addon->getUrl().'main')->setTitle(ovmlport_translate('Portlets OVML list')),
			$W->Action()->fromUrl($addon->getUrl().'main&idx=edit')->setTitle(ovmlport_translate('Add a OVML Portlet'))->setIcon(Func_Icons::ACTIONS_LIST_ADD)
		);
	}
}


/////////////////////////////////////////


class PortletDefinition_OVML implements portlet_PortletDefinitionInterface
{

	/**
	 * @var bab_addonInfos $addon
	 */
	protected $addon;
	private $id;
	private $name;
	private $description;

	public function __construct($id, $name, $description)
	{
		$this->id = 'ovml_'.$id;
		$this->name = $name;
		$this->description = $description;
		$this->addon = bab_getAddonInfosInstance('OVMLPortlets');
	}



	public function getId()
	{
		return $this->id;
	}


	public function getName()
	{
		return $this->name;
	}


	public function getDescription()
	{
		return $this->description;
	}


	public function getPortlet()
	{
		return new Portlet_OVML($this->id, $this->name, $this->description);
	}

	/**
	 * Returns the widget rich icon URL.
	 * 128x128 ?
	 *
	 * @return string
	 */
	public function getRichIcon()
	{
		return $this->addon->getStylePath() . 'images/icon48.png';
	}


	/**
	 * Returns the widget icon URL.
	 * 16x16 ?
	 *
	 * @return string
	 */
	public function getIcon()
	{
		return $this->addon->getStylePath() . 'images/icon48.png';
	}


	/**
	 * Get thumbnail URL
	 * max 120x60
	 */
	public function getThumbnail()
	{
		return '';
	}

	public function getPreferenceFields()
	{
		$portletId = str_replace('ovml_', '', $this->id);
		global $babDB;
		$res = $babDB->db_query("SELECT * from ovmlport_options a WHERE id_portlet = ".$babDB->quote($portletId));
		$options = array();
		while($arr = $babDB->db_fetch_array($res)){
			$options[] = array('label' => $arr['description'], 'type' => $arr['type'], 'name' => $arr['name']);
		}

		return $options;
	}

	public function getConfigurationActions()
	{
		if (!bab_isUserAdministrator()) {
			return array();
		}
		
		list(, $id_op) = explode('_', $this->getId());
		
		$addon = bab_getAddonInfosInstance('OVMLPortlets');
		
		$W = bab_functionality::get('Widgets');
		/*@var $W Func_Widgets */
		
		return array(
			$W->Action()->fromUrl($addon->getUrl().'main&idx=edit&id='.$id_op)->setTitle(ovmlport_translate('Edit'))->setIcon(Func_Icons::ACTIONS_DOCUMENT_EDIT),
			$W->Action()->fromUrl($addon->getUrl().'admin&idx=rights&item='.$id_op)->setTitle(ovmlport_translate('Set access rights'))->setIcon(Func_Icons::ACTIONS_SET_ACCESS_RIGHTS),
			$W->Action()->fromUrl($addon->getUrl().'admin&idx=remove&id='.$id_op)->setTitle(ovmlport_translate('Delete'))->setIcon(Func_Icons::ACTIONS_EDIT_DELETE)
		);
	}
}




class Portlet_OVML extends Widget_Item implements portlet_PortletInterface
{
	private $id;
	private $name;
	private $description;
	private $options = array();

	/**
	 * Instanciates the widget factory.
	 *
	 * @return Func_Widgets
	 */
	function Widgets()
	{
		return bab_Functionality::get('Widgets');
	}

	/**
	 */
	public function __construct($id, $name, $description)
	{
		$this->id = $id;
		$this->name = $name;
		$this->description = $description;
	}

	/**
	 * @param Widget_Canvas	$canvas
	 * @ignore
	 */
	public function display(Widget_Canvas $canvas)
	{
		$W = $this->Widgets();

		$portletId = str_replace('ovml_', '', $this->id);
		global $babDB;
		$res = $babDB->db_query("SELECT a.* from ovmlport_portlets a WHERE deleted = 0 AND id=".$babDB->quote($portletId));
		$arr = $babDB->db_fetch_assoc($res);
		
		// verify options
		
		$ropt = $babDB->db_query('SELECT name FROM ovmlport_options WHERE id_portlet='.$babDB->quote($arr['id']));
		while ($opt = $babDB->db_fetch_assoc($ropt))
		{
			if (!isset($this->options[$opt['name']]))
			{
				$this->setPreference($opt['name'], '');
			}
		}
		
		
		if(bab_isAccessValid('ovmlport_access_groups',$arr['id'])){
			if($arr['cached_time'] <= 0){
				$this->item = $W->Html(bab_printOvml($arr['ovml'], $this->options, 'OVMLPortlets '.$portletId));
			}else{
				$this->options['_ovml_cache_duration'] = $arr['cached_time'];
				$this->item = $W->Html(bab_printCachedOvml($arr['name'], $arr['ovml'], $this->options));
			}
		}else{
			$this->item = $W->Html('');
		}

		return $this->item->display($canvas);
	}

	public function getName()
	{
		return get_class($this);
	}

	public function getPortletDefinition()
	{
		return new PortletDefinition_OVML($this->id, $this->name, $this->description);
	}

	/**
	 * receive current user configuration from portlet API
	 */
	public function setPreferences(array $configuration)
	{
		$this->options = $configuration;
	}

	public function setPreference($name, $value)
	{
		$this->options[$name] = $value;
	}

	public function setPortletId($id)
	{

	}
}